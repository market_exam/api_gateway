package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"app/config"
	"app/genproto/auth_service"
	"app/genproto/catalog_service"
	"app/genproto/kassa_service"
	"app/genproto/organization_service"
	"app/genproto/stock_service"
	"app/genproto/user_service"
)

type ServiceManagerI interface {
	AuthService() auth_service.AuthServiceClient
	UserService() user_service.UserServiceClient

	// PRODUCT SERVICE

	CategoryService() catalog_service.CategoryServiceClient
	ProductService() catalog_service.ProductServiceClient

	// STOCK SERVICE

	OstatokService() stock_service.OstatokServiceClient
	ComingService() stock_service.ComingServiceClient
	ComingProductService() stock_service.ComingProductServiceClient

	// ORGANIZATION SERVICE

	FilialService() organization_service.FilialServiceClient
	CourierService() organization_service.CourierServiceClient
	EmployeeService() organization_service.EmployeeServiceClient
	SalePointService() organization_service.SalePointServiceClient

	// KASSA SERVICE

	SmenaService() kassa_service.SmenaServiceClient
	TransactionService() kassa_service.TransactionServiceClient
	SaleService() kassa_service.SaleServiceClient
	SalePaymentService() kassa_service.SalePaymentServiceClient
	SaleProductsService() kassa_service.SaleProductServiceClient
}

type grpcClients struct {
	authService    auth_service.AuthServiceClient
	userService    user_service.UserServiceClient

	categoryService catalog_service.CategoryServiceClient
	productService  catalog_service.ProductServiceClient

	stockService  stock_service.OstatokServiceClient
	comingService stock_service.ComingServiceClient
	comingProduct stock_service.ComingProductServiceClient

	branchService   organization_service.FilialServiceClient
	courierService  organization_service.CourierServiceClient
	employeeService organization_service.EmployeeServiceClient
	storeService    organization_service.SalePointServiceClient

	smenaService            kassa_service.SmenaServiceClient
	smenaTransactionService kassa_service.TransactionServiceClient
	saleProductService      kassa_service.SaleProductServiceClient
	saleService             kassa_service.SaleServiceClient
	salePaymentService      kassa_service.SalePaymentServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// Auth Service...
	connAuthService, err := grpc.Dial(
		cfg.AuthServiceHost+cfg.AuthGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Product Service...
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Ostatok Service...
	connStockService, err := grpc.Dial(
		cfg.StockServiceHost+cfg.StockGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Organization Service...
	connOrganizationService, err := grpc.Dial(
		cfg.OrganisationServiceHost+cfg.OrganisationGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Kassa Service...
	connKassaService, err := grpc.Dial(
		cfg.KassaServiceHost+cfg.KassaGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		authService:             auth_service.NewAuthServiceClient(connAuthService),
		userService:             user_service.NewUserServiceClient(connUserService),
		categoryService:         catalog_service.NewCategoryServiceClient(connProductService),
		productService:          catalog_service.NewProductServiceClient(connProductService),
		stockService:            stock_service.NewOstatokServiceClient(connStockService),
		comingService:           stock_service.NewComingServiceClient(connStockService),
		comingProduct:           stock_service.NewComingProductServiceClient(connStockService),
		branchService:           organization_service.NewFilialServiceClient(connOrganizationService),
		courierService:          organization_service.NewCourierServiceClient(connOrganizationService),
		employeeService:         organization_service.NewEmployeeServiceClient(connOrganizationService),
		storeService:            organization_service.NewSalePointServiceClient(connOrganizationService),
		smenaService:            kassa_service.NewSmenaServiceClient(connKassaService),
		smenaTransactionService: kassa_service.NewTransactionServiceClient(connKassaService),
		saleService:             kassa_service.NewSaleServiceClient(connKassaService),
		saleProductService:      kassa_service.NewSaleProductServiceClient(connKassaService),
		salePaymentService:      kassa_service.NewSalePaymentServiceClient(connKassaService),
	}, nil
}

func (g *grpcClients) AuthService() auth_service.AuthServiceClient {
	return g.authService
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

// Product Service
func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}

// Stock Service
func (g *grpcClients) OstatokService() stock_service.OstatokServiceClient {
	return g.stockService
}

func (g *grpcClients) ComingService() stock_service.ComingServiceClient {
	return g.comingService
}

func (g *grpcClients) ComingProductService() stock_service.ComingProductServiceClient {
	return g.comingProduct
}

// Organization Service

func (g *grpcClients) FilialService() organization_service.FilialServiceClient {
	return g.branchService
}

func (g *grpcClients) CourierService() organization_service.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) EmployeeService() organization_service.EmployeeServiceClient {
	return g.employeeService
}

func (g *grpcClients) SalePointService() organization_service.SalePointServiceClient {
	return g.storeService
}

// Kassa Service

func (g *grpcClients) SmenaService() kassa_service.SmenaServiceClient {
	return g.smenaService
}

func (g *grpcClients) TransactionService() kassa_service.TransactionServiceClient {
	return g.smenaTransactionService
}

func (g *grpcClients) SaleService() kassa_service.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductsService() kassa_service.SaleProductServiceClient {
	return g.saleProductService
}

func (g *grpcClients) SalePaymentService() kassa_service.SalePaymentServiceClient {
	return g.salePaymentService
}
