package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/organization_service"
	"app/pkg/util"
)

// CreateFilial godoc
// @ID create_filial
// @Router /filial [POST]
// @Summary Create Filial
// @Description  Create Filial
// @Tags Filial
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body organization_service.CreateFilial true "CreateFilialRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Filial} "GetFilialBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateFilial(c *gin.Context) {

	var filial organization_service.CreateFilial

	err := c.ShouldBindJSON(&filial)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.FilialService().Create(
		c.Request.Context(),
		&filial,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetFilialByID godoc
// @ID get_filial_by_id
// @Router /filial/{id} [GET]
// @Summary Get Filial  By ID
// @Description Get Filial  By ID
// @Tags Filial
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Filial} "FilialBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetFilialByID(c *gin.Context) {

	filialID := c.Param("id")

	if !util.IsValidUUID(filialID) {
		h.handleResponse(c, http.InvalidArgument, "filial id is an invalid uuid")
		return
	}

	resp, err := h.services.FilialService().GetByID(
		context.Background(),
		&organization_service.FilialPrimaryKey{
			Id: filialID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetFilialList godoc
// @ID get_filial_list
// @Router /filial [GET]
// @Summary Get Filial s List
// @Description  Get Filial s List
// @Tags Filial
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=organization_service.GetListFilialResponse} "GetAllFilialResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetFilialList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.FilialService().GetList(
		context.Background(),
		&organization_service.GetListFilialRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateFilial godoc
// @ID update_filial
// @Router /filial/{id} [PUT]
// @Summary Update Filial
// @Description Update Filial
// @Tags Filial
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateFilial true "UpdateFilialRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Filial} "Filial data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateFilial(c *gin.Context) {

	var filial organization_service.UpdateFilial

	filial.Id = c.Param("id")

	if !util.IsValidUUID(filial.Id) {
		h.handleResponse(c, http.InvalidArgument, "filial id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&filial)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.FilialService().Update(
		c.Request.Context(),
		&filial,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteFilial godoc
// @ID delete_filial
// @Router /filial/{id} [DELETE]
// @Summary Delete Filial
// @Description Delete Filial
// @Tags Filial
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Filial data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteFilial(c *gin.Context) {

	filialID := c.Param("id")

	if !util.IsValidUUID(filialID) {
		h.handleResponse(c, http.InvalidArgument, "filial id is an invalid uuid")
		return
	}

	resp, err := h.services.FilialService().Delete(
		c.Request.Context(),
		&organization_service.FilialPrimaryKey{Id: filialID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
