package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/kassa_service"
	"app/genproto/organization_service"
	"app/pkg/util"
)

// CreateSale godoc
// @ID create_sale
// @Router /sale [POST]
// @Summary Create Sale
// @Description  Create Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body kassa_service.CreateSale true "CreateSaleRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Sale} "GetSaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSale(c *gin.Context) {

	var sale kassa_service.CreateSale

	err := c.ShouldBindJSON(&sale)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	filial, err := h.services.FilialService().GetByID(context.Background(), &organization_service.FilialPrimaryKey{Id: sale.FilialId})

	sale.SaleCode = string(filial.FilialCode[0])

	resp, err := h.services.SaleService().Create(
		c.Request.Context(),
		&sale,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSaleByID godoc
// @ID get_sale_by_id
// @Router /sale/{id} [GET]
// @Summary Get Sale  By ID
// @Description Get Sale  By ID
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.Sale} "SaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleByID(c *gin.Context) {

	saleID := c.Param("id")

	if !util.IsValidUUID(saleID) {
		h.handleResponse(c, http.InvalidArgument, "sale id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleService().GetById(
		context.Background(),
		&kassa_service.SalePrimaryKey{
			Id: saleID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetSaleList godoc
// @ID get_sale_list
// @Router /sale [GET]
// @Summary Get Sale s List
// @Description  Get Sale s List
// @Tags Sale
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=kassa_service.GetListSaleResponse} "GetAllSaleResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SaleService().GetList(
		context.Background(),
		&kassa_service.GetListSaleRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateSale godoc
// @ID update_sale
// @Router /sale/{id} [PUT]
// @Summary Update Sale
// @Description Update Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateSale true "UpdateSaleRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Sale} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSale(c *gin.Context) {

	var sale kassa_service.UpdateSale

	sale.SaleId = c.Param("id")

	if !util.IsValidUUID(sale.SaleId) {
		h.handleResponse(c, http.InvalidArgument, "sale id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&sale)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleService().Update(
		c.Request.Context(),
		&sale,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSale godoc
// @ID delete_sale
// @Router /sale/{id} [DELETE]
// @Summary Delete Sale
// @Description Delete Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSale(c *gin.Context) {

	saleID := c.Param("id")

	if !util.IsValidUUID(saleID) {
		h.handleResponse(c, http.InvalidArgument, "sale id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleService().Delete(
		c.Request.Context(),
		&kassa_service.SalePrimaryKey{Id: saleID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
