package handlers

import (
	"bufio"
	"encoding/json"
	"errors"
	htp "net/http"
	"net/url"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gogo/protobuf/jsonpb"
	"google.golang.org/protobuf/runtime/protoiface"

	"app/api/http"
	"app/api/models"
	"app/config"
	"app/pkg/logger"
	"app/services"
)

type Handler struct {
	cfg      config.Config
	log      logger.LoggerI
	services services.ServiceManagerI
}

func NewHandler(cfg config.Config, log logger.LoggerI, svcs services.ServiceManagerI) Handler {
	return Handler{
		cfg:      cfg,
		log:      log,
		services: svcs,
	}
}

func (h *Handler) handleResponse(c *gin.Context, status http.Status, data interface{}) {
	switch code := status.Code; {
	case code < 300:
		h.log.Info(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			// logger.Any("data", data),
		)
	case code < 400:
		h.log.Warn(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	default:
		h.log.Error(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	}

	c.JSON(status.Code, data)
}

func ProtoToStruct(s interface{}, p protoiface.MessageV1) error {
	var jm jsonpb.Marshaler

	jm.EmitDefaults = true
	jm.OrigName = true

	ms, err := jm.MarshalToString(p)

	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(ms), &s)

	return err
}

func (h *Handler) getOffsetParam(c *gin.Context) (offset int, err error) {
	if h.cfg.DefaultOffset != "" {
		offsetStr := c.DefaultQuery("offset", h.cfg.DefaultOffset)
		return strconv.Atoi(offsetStr)
	}

	offsetStr := c.DefaultQuery("offset", "0")
	return strconv.Atoi(offsetStr)
}

func (h *Handler) getLimitParam(c *gin.Context) (offset int, err error) {
	if h.cfg.DefaultLimit != "" {
		limitStr := c.DefaultQuery("limit", h.cfg.DefaultLimit)
		return strconv.Atoi(limitStr)
	}
	limitStr := c.DefaultQuery("limit", "10")
	return strconv.Atoi(limitStr)
}

func (h *Handler) handleErrorResponse(c *gin.Context, code int, message string, err interface{}) {
	h.log.Error(message, logger.Int("code", code), logger.Any("error", err))
	c.JSON(code, ResponseModel{
		Code:    code,
		Message: message,
		Error:   err,
	})
}

func (h *Handler) handleSuccessResponse(c *gin.Context, code int, message string, data interface{}) {
	c.JSON(code, ResponseModel{
		Code:    code,
		Message: message,
		Data:    data,
	})
}

func (h *Handler) MakeProxy(c *gin.Context, proxyUrl, path string) (err error) {
	req := c.Request

	proxy, err := url.Parse(proxyUrl)
	if err != nil {
		h.log.Error("error in parse addr: %v", logger.Error(err))
		c.String(htp.StatusInternalServerError, "error")
		return
	}

	req.URL.Scheme = proxy.Scheme
	req.URL.Host = proxy.Host
	req.URL.Path = path
	transport := htp.DefaultTransport
	resp, err := transport.RoundTrip(req)
	if err != nil {
		h.log.Error("error while transport: %v", logger.Error(err))
		return
	}

	for k, vv := range resp.Header {
		for _, v := range vv {
			c.Header(k, v)
		}
	}
	defer resp.Body.Close()

	c.Status(resp.StatusCode)
	_, _ = bufio.NewReader(resp.Body).WriteTo(c.Writer)
	return
}

func getUserInfo(h *Handler, c *gin.Context, info *models.UserInfo) error {
	var (
		ErrUnauthorized = errors.New("Unauthorized")
		accessToken     string
	)

	accessToken = c.GetHeader("Authorization")
	if accessToken == "" {
		c.JSON(htp.StatusUnauthorized, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		h.log.Error("Unauthorized request: ", logger.Error(ErrUnauthorized))
		return errors.New("unauthorized request")
	}

	// resp, err := h.services.AuthService().CheckPermission(context.Background(), &auth_service.CheckPermissionRequest{
	// 	AccessToken: accessToken,
	// })
	// if err != nil {
	// 	c.JSON(htp.StatusUnauthorized, models.ResponseError{
	// 		Error: ErrorCodeUnauthorized,
	// 	})
	// 	h.log.Error("Unauthorized request: ", logger.Error(err))
	// 	return errors.New("unauthorized request")
	// }

	// if !resp.HasPermission {
	// 	c.JSON(htp.StatusForbidden, models.ResponseError{
	// 		Error: ErrorCodeForbidden,
	// 	})
	// 	h.log.Error("Request Forbidden")
	// 	return errors.New("request Forbidden")
	// }
	// info.UserID = resp.UserId

	return nil
}

const (
	// ErrorCodeInvalidURL ...
	ErrorCodeInvalidURL = "INVALID_URL"
	// ErrorCodeInvalidJSON ...
	ErrorCodeInvalidJSON = "INVALID_JSON"
	// ErrorCodeInternal ...
	ErrorCodeInternal = "INTERNAL"
	// ErrorCodeUnauthorized ...
	ErrorCodeUnauthorized = "UNAUTHORIZED"
	// ErrorCodeAlreadyExists ...
	ErrorCodeAlreadyExists = "ALREADY_EXISTS"
	// ErrorCodeNotFound ...
	ErrorCodeNotFound = "NOT_FOUND"
	// ErrorCodeInvalidCode ...
	ErrorCodeInvalidCode = "INVALID_CODE"
	// ErrorBadRequest ...
	ErrorBadRequest = "BAD_REQUEST"
	// ErrorCodeForbidden ...
	ErrorCodeForbidden = "FORBIDDEN"
	// ErrorCodeNotApproved ...
	ErrorCodeNotApproved = "NOT_APPROVED"
	// ErrorCodeWrongClub ...
	ErrorCodeWrongClub = "WRONG_CLUB"
	// ErrorCodePasswordsNotEqual ...
	ErrorCodePasswordsNotEqual = "PASSWORDS_NOT_EQUAL"
)

var (
	SigningKey = []byte("FfLbN7pIEYe8@!EqrttOLiwa(H8)7Ddo")
)
