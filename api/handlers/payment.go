package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/kassa_service"
	"app/pkg/util"
)

// CreateSalePayment godoc
// @ID create_salePayment
// @Router /salePayment [POST]
// @Summary Create SalePayment
// @Description  Create SalePayment
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body kassa_service.CreateSalePayment true "CreateSalePaymentRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SalePayment} "GetSalePaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSalePayment(c *gin.Context) {

	var salePayment kassa_service.CreateSalePayment

	err := c.ShouldBindJSON(&salePayment)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SalePaymentService().Create(
		c.Request.Context(),
		&salePayment,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSalePaymentByID godoc
// @ID get_salePayment_by_id
// @Router /salePayment/{id} [GET]
// @Summary Get SalePayment  By ID
// @Description Get SalePayment  By ID
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.SalePayment} "SalePaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalePaymentByID(c *gin.Context) {

	paymentID := c.Param("id")

	if !util.IsValidUUID(paymentID) {
		h.handleResponse(c, http.InvalidArgument, "salePayment id is an invalid uuid")
		return
	}

	resp, err := h.services.SalePaymentService().GetById(
		context.Background(),
		&kassa_service.SalePaymentPrimaryKey{
			Id: paymentID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetSalePaymentList godoc
// @ID get_salePayment_list
// @Router /salePayment [GET]
// @Summary Get SalePayment s List
// @Description  Get SalePayment s List
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=kassa_service.GetListSalePaymentResponse} "GetAllSalePaymentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalePaymentList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SalePaymentService().GetList(
		context.Background(),
		&kassa_service.GetListSalePaymentRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateSalePayment godoc
// @ID update_salePayment
// @Router /salePayment/{id} [PUT]
// @Summary Update SalePayment
// @Description Update SalePayment
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateSalePayment true "UpdateSalePaymentRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SalePayment} "SalePayment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSalePayment(c *gin.Context) {

	var salePayment kassa_service.UpdateSalePayment

	salePayment.Id = c.Param("id")

	if !util.IsValidUUID(salePayment.Id) {
		h.handleResponse(c, http.InvalidArgument, "salePayment id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&salePayment)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SalePaymentService().Update(
		c.Request.Context(),
		&salePayment,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSalePayment godoc
// @ID delete_salePayment
// @Router /salePayment/{id} [DELETE]
// @Summary Delete SalePayment
// @Description Delete SalePayment
// @Tags SalePayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SalePayment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSalePayment(c *gin.Context) {

	paymentID := c.Param("id")

	if !util.IsValidUUID(paymentID) {
		h.handleResponse(c, http.InvalidArgument, "salePayment id is an invalid uuid")
		return
	}

	resp, err := h.services.SalePaymentService().Delete(
		c.Request.Context(),
		&kassa_service.SalePaymentPrimaryKey{Id: paymentID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
