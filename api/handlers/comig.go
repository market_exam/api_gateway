package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/stock_service"
	"app/pkg/util"
)

// CreateComing godoc
// @ID create_coming
// @Router /coming [POST]
// @Summary Create Coming
// @Description  Create Coming
// @Tags Coming
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body stock_service.CreateComing true "CreateComingRequestBody"
// @Success 200 {object} http.Response{data=stock_service.Coming} "GetComingBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateComing(c *gin.Context) {

	var coming stock_service.CreateComing

	err := c.ShouldBindJSON(&coming)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingService().Create(
		c.Request.Context(),
		&coming,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetComingByID godoc
// @ID get_coming_by_id
// @Router /coming/{id} [GET]
// @Summary Get Coming  By ID
// @Description Get Coming  By ID
// @Tags Coming
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=stock_service.Coming} "ComingBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingByID(c *gin.Context) {

	comingID := c.Param("id")

	if !util.IsValidUUID(comingID) {
		h.handleResponse(c, http.InvalidArgument, "coming id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingService().GetById(
		context.Background(),
		&stock_service.ComingPrimaryKey{
			Id: comingID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetComingList godoc
// @ID get_coming_list
// @Router /coming [GET]
// @Summary Get Coming s List
// @Description  Get Coming s List
// @Tags Coming
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=stock_service.GetListComingResponse} "GetAllComingResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ComingService().GetList(
		context.Background(),
		&stock_service.GetListComingRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateComing godoc
// @ID update_coming
// @Router /coming/{id} [PUT]
// @Summary Update Coming
// @Description Update Coming
// @Tags Coming
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body stock_service.UpdateComing true "UpdateComingRequestBody"
// @Success 200 {object} http.Response{data=stock_service.Coming} "Coming data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateComing(c *gin.Context) {

	var coming stock_service.UpdateComing

	coming.Id = c.Param("id")

	if !util.IsValidUUID(coming.Id) {
		h.handleResponse(c, http.InvalidArgument, "coming id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&coming)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingService().Update(
		c.Request.Context(),
		&coming,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteComing godoc
// @ID delete_coming
// @Router /coming/{id} [DELETE]
// @Summary Delete Coming
// @Description Delete Coming
// @Tags Coming
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Coming data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteComing(c *gin.Context) {

	comingID := c.Param("id")

	if !util.IsValidUUID(comingID) {
		h.handleResponse(c, http.InvalidArgument, "coming id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingService().Delete(
		c.Request.Context(),
		&stock_service.ComingPrimaryKey{Id: comingID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
