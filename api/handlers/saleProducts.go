package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/kassa_service"
	"app/genproto/stock_service"
	"app/pkg/util"
)

// CreateSaleProduct godoc
// @ID create_saleProduct
// @Router /saleProduct [POST]
// @Summary Create SaleProduct
// @Description  Create SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body kassa_service.CreateSaleProduct true "CreateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SaleProduct} "GetSaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSaleProduct(c *gin.Context) {

	var saleProduct kassa_service.CreateSaleProduct

	err := c.ShouldBindJSON(&saleProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	product_in_ostatok, err := h.services.OstatokService().GetProduct(context.Background(), &stock_service.OstatokPrimaryKey{Id: saleProduct.ProductId})
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	if product_in_ostatok.Count < saleProduct.Count {
		h.handleResponse(c, http.BadRequest, "not enough in stock")
		return
	}

	resp, err := h.services.SaleProductsService().Create(
		c.Request.Context(),
		&saleProduct,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSaleProductByID godoc
// @ID get_saleProduct_by_id
// @Router /saleProduct/{id} [GET]
// @Summary Get SaleProduct  By ID
// @Description Get SaleProduct  By ID
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.SaleProduct} "SaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductByID(c *gin.Context) {

	saleProductID := c.Param("id")

	if !util.IsValidUUID(saleProductID) {
		h.handleResponse(c, http.InvalidArgument, "saleProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleProductsService().GetById(
		context.Background(),
		&kassa_service.SaleProductPrimaryKey{
			SaleProductId: saleProductID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetSaleProductList godoc
// @ID get_saleProduct_list
// @Router /saleProduct [GET]
// @Summary Get SaleProduct s List
// @Description  Get SaleProduct s List
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=kassa_service.GetListSaleProductResponse} "GetAllSaleProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SaleProductsService().GetList(
		context.Background(),
		&kassa_service.GetListSaleProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateSaleProduct godoc
// @ID update_saleProduct
// @Router /saleProduct/{id} [PUT]
// @Summary Update SaleProduct
// @Description Update SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body kassa_service.UpdateSaleProduct true "UpdateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.SaleProduct} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSaleProduct(c *gin.Context) {

	var saleProduct kassa_service.UpdateSaleProduct

	saleProduct.SaleProductId = c.Param("id")

	if !util.IsValidUUID(saleProduct.SaleProductId) {
		h.handleResponse(c, http.InvalidArgument, "saleProduct id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&saleProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductsService().Update(
		c.Request.Context(),
		&saleProduct,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSaleProduct godoc
// @ID delete_saleProduct
// @Router /saleProduct/{id} [DELETE]
// @Summary Delete SaleProduct
// @Description Delete SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSaleProduct(c *gin.Context) {

	saleProductID := c.Param("id")

	if !util.IsValidUUID(saleProductID) {
		h.handleResponse(c, http.InvalidArgument, "saleProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.SaleProductsService().Delete(
		c.Request.Context(),
		&kassa_service.SaleProductPrimaryKey{SaleProductId: saleProductID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
