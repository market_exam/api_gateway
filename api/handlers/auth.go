package handlers

import (
	"app/api/models"
	"app/genproto/auth_service"
	"app/pkg/logger"
	"context"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gogo/protobuf/jsonpb"
)

// @Router /user/login [POST]
// @Summary User Login
// @Description API that checks whether customer exists
// @Description and if exists sends sms to their number
// @Tags auth
// @Accept  json
// @Produce  json
// @Param login body models.CustomerLoginRequest true "login"
// @Success 200 {object} models.ResponseOK
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *Handler) CustomerLogin(c *gin.Context) {
	var (
		login models.CustomerLoginRequest
	)

	err := c.ShouldBindJSON(&login)
	if err != nil {
		log.Println("error while binding json")
		return
	}

	_, err = h.services.AuthService().CustomerLogin(context.Background(), &auth_service.OTPLoginRequest{
		Phone: login.Phone,
		Tag:   login.Tag,
	})
	if err != nil {
		log.Println("error while logging in")
		c.JSON(404, models.ResponseOK{
			Message: "Not Found",
		})
		return
	}

	c.JSON(200, models.ResponseOK{
		Message: "Code has been sent",
	})
}

// @Router /user/confirm-login [POST]
// @Summary Confirm Customer Login
// @Description API that confirms sms code
// @Tags auth
// @Accept  json
// @Produce  json
// @Param confirm_phone body models.ConfirmCustomerLoginRequest true "confirm login"
// @Success 200 {object} models.GetCustomerModel
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *Handler) ConfirmCustomerLogin(c *gin.Context) {
	var (
		cm          models.ConfirmCustomerLoginRequest
		jspbMarshal jsonpb.Marshaler
	)

	jspbMarshal.OrigName = true
	jspbMarshal.EmitDefaults = true

	err := c.ShouldBindJSON(&cm)
	if err != nil {
		log.Println("error while binding json")
		return
	}

	resp, err := h.services.AuthService().CustomerConfirmLogin(context.Background(), &auth_service.OTPConfirmRequest{
		FirstName:   cm.FirstName,
		LastName:    cm.LastName,
		Description: cm.Description,
		Phone:       cm.Phone,
		Code:        cm.Code,
		DateOfBirth: cm.DateOfBirth,
		Gender:      cm.Gender,
	})

	if err != nil {
		log.Println("error while logging")
		c.String(http.StatusOK, "NotFoundUser")
		return
	}

	js, err := jspbMarshal.MarshalToString(resp)
	if err != nil {
		log.Println("error while marshalling")
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}

// @Summary Register
// @Description Register - API for registering customers
// @Tags auth
// @Accept  json
// @Produce  json
// @Param register body models.RegisterModel true "register"
// @Success 200 {object} models.ResponseOK
// @Failure 400 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
// @Router /user/register [post]
func (h *Handler) CustomerRegister(c *gin.Context) {
	var (
		reg models.RegisterModel
	)

	err := c.ShouldBindJSON(&reg)
	if err != nil {
		log.Println("error binding json")
		return
	}

	// err = helpers.ValidatePhone(reg.Phone)
	// if err != nil {
	// 	c.JSON(http.StatusBadRequest, models.ResponseError{
	// 		Error: err.Error(),
	// 	})
	// 	return
	// }

	_, err = h.services.AuthService().CustomerRegister(context.Background(), &auth_service.OTPRegisterRequest{
		Phone: reg.Phone,
		Name:  reg.Name,
	})
	if err != nil {
		log.Println("error while registering")
		return
	}
	c.JSON(200, models.ResponseOK{
		Message: "Code has been sent",
	})
}

// @Router /user/register-confirm [post]
// @Summary Register confirm
// @Description Register - API for confirming and inserting user to DB
// @Tags auth
// @Accept  json
// @Produce  json
// @Param register_confirm body models.RegisterConfirmModel true "register_confirm"
// @Success 200 {object} models.GetCustomerModel
// @Failure 400 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *Handler) CustomerRegisterConfirm(c *gin.Context) {
	var (
		jspbMarshal jsonpb.Marshaler
		reg         models.RegisterConfirmModel
	)

	jspbMarshal.OrigName = true
	jspbMarshal.EmitDefaults = true

	err := c.ShouldBindJSON(&reg)
	if err != nil {
		log.Println("error whilebinding json")
		return
	}

	resp, err := h.services.AuthService().CustomerConfirmRegister(context.Background(), &auth_service.OTPConfirmRequest{
		FirstName:   reg.FirstName,
		LastName:    reg.LastName,
		Description: reg.Description,
		Phone:       reg.Phone,
		Code:        reg.Code,
		DateOfBirth: reg.DateOfBirth,
		Gender:      reg.Gender,
	})
	if err != nil {
		log.Println("error while logging in")
		return
	}
	js, err := jspbMarshal.MarshalToString(resp)
	if err != nil {
		log.Println("error while marshalling")
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}

// @Security ApiKeyAuth
// @Router /user/refresh-token [post]
// @Summary Customer Refresh Token
// @Description Customer refresh token
// @Tags auth
// @Accept  json
// @Produce  json
// @Param refresh_token body models.RefreshTokenRequest true "refresh-token"
// @Success 200 {object} models.RefreshTokenResponse
// @Failure 400 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
func (h *Handler) CustomerRefreshToken(c *gin.Context) {
	var (
		refreshTokenRequest models.RefreshTokenRequest
		jspbMarshal         jsonpb.Marshaler
	)

	jspbMarshal.OrigName = true
	jspbMarshal.EmitDefaults = true

	err := c.ShouldBindJSON(&refreshTokenRequest)
	if err != nil {
		h.log.Error("error while binding refresh token request parameters", logger.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "refresh token field is required ",
			"code":    "400",
		})
		return
	}

	resp, err := h.services.AuthService().RefreshToken(context.Background(), &auth_service.RefreshTokenRequest{
		RefreshToken: refreshTokenRequest.RefreshToken,
	})
	if err != nil {
		log.Println("error while logging ing")
		return
	}

	js, err := jspbMarshal.MarshalToString(resp)
	if err != nil {
		log.Println("Error while marshallling")
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}
