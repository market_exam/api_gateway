package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/stock_service"
	"app/pkg/util"
)

// CreateOstatok godoc
// @ID create_ostatok
// @Router /ostatok [POST]
// @Summary Create Ostatok
// @Description  Create Ostatok
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body stock_service.CreateOstatok true "CreateOstatokRequestBody"
// @Success 200 {object} http.Response{data=stock_service.Ostatok} "GetOstatokBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateOstatok(c *gin.Context) {

	var ostatok stock_service.CreateOstatok

	err := c.ShouldBindJSON(&ostatok)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OstatokService().Create(
		c.Request.Context(),
		&ostatok,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetOstatokByID godoc
// @ID get_ostatok_by_id
// @Router /ostatok/{id} [GET]
// @Summary Get Ostatok  By ID
// @Description Get Ostatok  By ID
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=stock_service.Ostatok} "OstatokBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOstatokByID(c *gin.Context) {

	ostatokID := c.Param("id")

	if !util.IsValidUUID(ostatokID) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	resp, err := h.services.OstatokService().GetById(
		context.Background(),
		&stock_service.OstatokPrimaryKey{
			Id: ostatokID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetOstatokList godoc
// @ID get_ostatok_list
// @Router /ostatok [GET]
// @Summary Get Ostatok s List
// @Description  Get Ostatok s List
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=stock_service.GetListOstatokResponse} "GetAllOstatokResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOstatokList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.OstatokService().GetList(
		context.Background(),
		&stock_service.GetListOstatokRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateOstatok godoc
// @ID update_ostatok
// @Router /ostatok/{id} [PUT]
// @Summary Update Ostatok
// @Description Update Ostatok
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body stock_service.UpdateOstatok true "UpdateOstatokRequestBody"
// @Success 200 {object} http.Response{data=stock_service.Ostatok} "Ostatok data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateOstatok(c *gin.Context) {

	var ostatok stock_service.UpdateOstatok

	ostatok.Id = c.Param("id")

	if !util.IsValidUUID(ostatok.Id) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&ostatok)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.OstatokService().Update(
		c.Request.Context(),
		&ostatok,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteOstatok godoc
// @ID delete_ostatok
// @Router /ostatok/{id} [DELETE]
// @Summary Delete Ostatok
// @Description Delete Ostatok
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Ostatok data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteOstatok(c *gin.Context) {

	ostatokID := c.Param("id")

	if !util.IsValidUUID(ostatokID) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	resp, err := h.services.OstatokService().Delete(
		c.Request.Context(),
		&stock_service.OstatokPrimaryKey{Id: ostatokID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// GetOstatokByProductId godoc
// @ID get_ostatok_by_product_id
// @Router /ostatok_product_id/{id} [GET]
// @Summary Get Ostatok  By ProductId
// @Description Get Ostatok  By ProductId
// @Tags Ostatok
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=stock_service.Ostatok} "OstatokBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOstatokByProductId(c *gin.Context) {

	ostatokID := c.Param("id")

	if !util.IsValidUUID(ostatokID) {
		h.handleResponse(c, http.InvalidArgument, "ostatok id is an invalid uuid")
		return
	}

	resp, err := h.services.OstatokService().GetProduct(
		context.Background(),
		&stock_service.OstatokPrimaryKey{
			Id: ostatokID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
