package handlers

import (
	"app/genproto/stock_service"
	"context"
	"market_exam/api_gateway/api/http"

	"github.com/gin-gonic/gin"
)

// Coming_to_Stock godoc
// @ID coming_to_stock
// @Router /coming_to_stock/{id} [POST]
// @Summary Coming To Stock
// @Description  Coming To Stock
// @Tags Business Events
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=stock_service.Stock} "GetStockBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Coming_to_Stock(c *gin.Context) {

	coming_id := c.Param("id")

	coming_products, err := h.services.ComingProductService().GetList(
		context.Background(),
		&stock_service.GetListComingProductRequest{
			Offset: 0,
			Limit:  0,
			Search: coming_id,
		},
	)

	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	coming, err := h.services.ComingService().GetById(
		context.Background(),
		&stock_service.ComingPrimaryKey{
			Id: coming_id,
		},
	)

	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	products_in_stock, err := h.services.OstatokService().GetList(
		context.Background(),
		&stock_service.GetListOstatokRequest{
			Offset: 0,
			Limit:  0,
			Search: coming.FilialId,
		},
	)

	if err != nil {
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	flag := false

	for _, coming_product := range coming_products.ComingProducts {

		flag = false

		for _, product_in_stock := range products_in_stock.Ostatoks {

			if product_in_stock.ProductId == coming_product.ProductId {

				_, err := h.services.OstatokService().Update(
					context.Background(),
					&stock_service.UpdateOstatok{
						Id:          product_in_stock.Id,
						FilialId:    product_in_stock.getfilialid(),
						CategoryId:  product_in_stock.GetCategoryId(),
						ProductId:   product_in_stock.GetProductId(),
						Count:       (product_in_stock.GetCount() + int64(coming_product.GetCount())),
						Barcode:     product_in_stock.GetBarcode(),
						ComingPrice: product_in_stock.GetComingPrice(),
					},
				)

				flag = true

				if err != nil {
					h.handleResponse(c, http.InternalServerError, err.Error())
					return
				}

			}

		}

		if !flag {

			_, err := h.services.OstatokService().Create(
				context.Background(),
				&stock_service.CreateOstatok{
					FilialId:    coming.GetBranchId(),
					CategoryId:  coming_product.GetCategoryId(),
					ProductId:   coming_product.GetProductId(),
					Count:       int64(coming_product.Count()),
					Barcode:     coming_product.GetBarcode(),
					ComingPrice: coming_product.GetComingPrice(),
				},
			)

			if err != nil {
				h.handleResponse(c, http.InternalServerError, err.Error())
				return
			}
		}

	}

}
