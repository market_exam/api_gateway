package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/kassa_service"
	"app/pkg/util"
)

// Transaction godoc
// @ID create_transaction
// @Router /transaction [POST]
// @Summary Create Transaction
// @Description  Create Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body kassa_service.Transaction true "TransactionRequestBody"
// @Success 200 {object} http.Response{data=kassa_service.Transaction} "GetTransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateTransaction(c *gin.Context) {


	var transaction kassa_service.CreateTransaction

	err := c.ShouldBindJSON(&transaction)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.TransactionService().Create(
		c.Request.Context(),
		&transaction,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetTransactionByID godoc
// @ID get_transaction_by_id
// @Router /transaction/{id} [GET]
// @Summary Get Transaction  By ID
// @Description Get Transaction  By ID
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=kassa_service.Transaction} "TransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionByID(c *gin.Context) {

	transactionID := c.Param("id")

	if !util.IsValidUUID(transactionID) {
		h.handleResponse(c, http.InvalidArgument, "transaction id is an invalid uuid")
		return
	}

	resp, err := h.services.TransactionService().GetById(
		context.Background(),
		&kassa_service.TransactionPrimaryKey{
			Id: transactionID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetTransactionList godoc
// @ID get_transaction_list
// @Router /transaction [GET]
// @Summary Get Transaction s List
// @Description  Get Transaction s List
// @Tags Transaction
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=kassa_service.GetListTransactionResponse} "GetAllTransactionResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTransactionList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.TransactionService().GetList(
		context.Background(),
		&kassa_service.GetListTransactionRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteTransaction godoc
// @ID delete_transaction
// @Router /transaction/{id} [DELETE]
// @Summary Delete Transaction
// @Description Delete Transaction
// @Tags Transaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Transaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteTransaction(c *gin.Context) {

	transactionID := c.Param("id")

	if !util.IsValidUUID(transactionID) {
		h.handleResponse(c, http.InvalidArgument, "transaction id is an invalid uuid")
		return
	}

	resp, err := h.services.TransactionService().Delete(
		c.Request.Context(),
		&kassa_service.TransactionPrimaryKey{Id: transactionID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
