package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/stock_service"
	"app/pkg/util"
)

// CreateComingProduct godoc
// @ID create_comingProduct
// @Router /comingProduct [POST]
// @Summary Create ComingProduct
// @Description  Create ComingProduct
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body stock_service.CreateComingProduct true "CreateComingProductRequestBody"
// @Success 200 {object} http.Response{data=stock_service.ComingProduct} "GetComingProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateComingProduct(c *gin.Context) {

	var comingProduct stock_service.CreateComingProduct

	err := c.ShouldBindJSON(&comingProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().Create(
		c.Request.Context(),
		&comingProduct,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetComingProductByID godoc
// @ID get_comingProduct_by_id
// @Router /comingProduct/{id} [GET]
// @Summary Get ComingProduct  By ID
// @Description Get ComingProduct  By ID
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=stock_service.ComingProduct} "ComingProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingProductByID(c *gin.Context) {

	comingProductID := c.Param("id")

	if !util.IsValidUUID(comingProductID) {
		h.handleResponse(c, http.InvalidArgument, "comingProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingProductService().GetById(
		context.Background(),
		&stock_service.ComingProductPrimaryKey{
			Id: comingProductID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetComingProductList godoc
// @ID get_comingProduct_list
// @Router /comingProduct [GET]
// @Summary Get ComingProduct s List
// @Description  Get ComingProduct s List
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=stock_service.GetListComingProductResponse} "GetAllComingProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingProductList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().GetList(
		context.Background(),
		&stock_service.GetListComingProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateComingProduct godoc
// @ID update_comingProduct
// @Router /comingProduct/{id} [PUT]
// @Summary Update ComingProduct
// @Description Update ComingProduct
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body stock_service.UpdateComingProduct true "UpdateComingProductRequestBody"
// @Success 200 {object} http.Response{data=stock_service.ComingProduct} "ComingProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateComingProduct(c *gin.Context) {

	var comingProduct stock_service.UpdateComingProduct

	comingProduct.Id = c.Param("id")

	if !util.IsValidUUID(comingProduct.Id) {
		h.handleResponse(c, http.InvalidArgument, "comingProduct id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&comingProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ComingProductService().Update(
		c.Request.Context(),
		&comingProduct,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteComingProduct godoc
// @ID delete_comingProduct
// @Router /comingProduct/{id} [DELETE]
// @Summary Delete ComingProduct
// @Description Delete ComingProduct
// @Tags ComingProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ComingProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteComingProduct(c *gin.Context) {

	comingProductID := c.Param("id")

	if !util.IsValidUUID(comingProductID) {
		h.handleResponse(c, http.InvalidArgument, "comingProduct id is an invalid uuid")
		return
	}

	resp, err := h.services.ComingProductService().Delete(
		c.Request.Context(),
		&stock_service.ComingProductPrimaryKey{Id: comingProductID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
