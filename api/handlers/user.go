package handlers

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/api/models"
	"app/genproto/user_service"
	"app/pkg/util"
)

// CreateUser godoc
// @ID create_user
// @Router /user [POST]
// @Summary Create User
// @Description  Create User
// @Tags User
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateUser true "CreateUserRequestBody"
// @Success 200 {object} http.Response{data=user_service.User} "GetUserBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateUser(c *gin.Context) {
	var (
		userInfo models.UserInfo
	)

	if c.GetHeader("Authorization") == "" {
		c.JSON(http.Unauthorized.Code, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	if c.GetHeader("Authorization") != "" {
		err := getUserInfo(h, c, &userInfo)
		if err != nil {
			return
		}

	}
	var user user_service.CreateUser

	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.UserService().Create(
		c.Request.Context(),
		&user,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// @Security ApiKeyAuth
// GetUserByID godoc
// @ID get_user1_by_id
// @Router /user/{id} [GET]
// @Summary Get User  By ID
// @Description Get User  By ID
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.User} "UserBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetUserByID(c *gin.Context) {
	var (
		userInfo models.UserInfo
	)

	if c.GetHeader("Authorization") == "" {
		c.JSON(http.Unauthorized.Code, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	if c.GetHeader("Authorization") != "" {
		err := getUserInfo(h, c, &userInfo)
		if err != nil {
			return
		}

	}
	userID := c.Param("id")

	if !util.IsValidUUID(userID) {
		h.handleResponse(c, http.InvalidArgument, "user id is an invalid uuid")
		return
	}

	resp, err := h.services.UserService().GetByPKey(
		context.Background(),
		&user_service.UserPrimaryKey{
			Id: userID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetUserList godoc
// @ID get_user_list
// @Router /user [GET]
// @Summary Get User s List
// @Description  Get User s List
// @Tags User
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetAllUserResponse} "GetAllUserResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetUserList(c *gin.Context) {
	var (
		userInfo models.UserInfo
	)

	if c.GetHeader("Authorization") == "" {
		c.JSON(http.Unauthorized.Code, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	if c.GetHeader("Authorization") != "" {
		err := getUserInfo(h, c, &userInfo)
		if err != nil {
			return
		}

	}
	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.UserService().GetAll(
		context.Background(),
		&user_service.GetAllUserRequest{
			Limit:  uint64(limit),
			Offset: uint64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// UpdateUser godoc
// @ID update_user
// @Router /user/{id} [PUT]
// @Summary Update User
// @Description Update User
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateUser true "UpdateUserRequestBody"
// @Success 200 {object} http.Response{data=user_service.User} "User data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateUser(c *gin.Context) {
	var (
		userInfo models.UserInfo
	)

	if c.GetHeader("Authorization") == "" {
		c.JSON(http.Unauthorized.Code, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	if c.GetHeader("Authorization") != "" {
		err := getUserInfo(h, c, &userInfo)
		if err != nil {
			return
		}

	}
	var user user_service.UpdateUser

	user.Id = c.Param("id")

	if !util.IsValidUUID(user.Id) {
		h.handleResponse(c, http.InvalidArgument, "user id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.UserService().Update(
		c.Request.Context(),
		&user,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// DeleteUser godoc
// @ID delete_user1
// @Router /user/{id} [DELETE]
// @Summary Delete User
// @Description Delete User
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "User data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteUser(c *gin.Context) {
	var (
		userInfo models.UserInfo
	)

	if c.GetHeader("Authorization") == "" {
		c.JSON(http.Unauthorized.Code, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	if c.GetHeader("Authorization") != "" {
		err := getUserInfo(h, c, &userInfo)
		if err != nil {
			return
		}

	}
	userID := c.Param("id")

	if !util.IsValidUUID(userID) {
		h.handleResponse(c, http.InvalidArgument, "user id is an invalid uuid")
		return
	}

	resp, err := h.services.UserService().Delete(
		c.Request.Context(),
		&user_service.UserPrimaryKey{Id: userID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}

// @Security ApiKeyAuth
// GetUserOrderPaymentList godoc
// @ID get_user_payment_list
// @Router /user-payment-history/{id} [GET]
// @Summary Get User s List
// @Description  Get User s List
// @Tags UserHistory
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.GetAllUserResponse} "GetAllUserResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetUserPaymentHistory(c *gin.Context) {
	var (
		userInfo models.UserInfo
	)

	if c.GetHeader("Authorization") == "" {
		c.JSON(http.Unauthorized.Code, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	if c.GetHeader("Authorization") != "" {
		err := getUserInfo(h, c, &userInfo)
		if err != nil {
			return
		}

	}
	userId := c.Param("id")
	fmt.Println("ID: ", userId)
	resp, err := h.services.UserService().GetUserPaymentHistory(context.Background(), &user_service.UserPrimaryKey{
		Id: userId,
	})

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetUserOrderHistoryList godoc
// @ID get_user_order_list
// @Router /user-order-history/{id} [GET]
// @Summary Get User s List
// @Description  Get User s List
// @Tags UserHistory
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.GetAllUserResponse} "GetAllUserResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetUserOrderHistory(c *gin.Context) {
	var (
		userInfo models.UserInfo
	)

	if c.GetHeader("Authorization") == "" {
		c.JSON(http.Unauthorized.Code, models.ResponseError{
			Error: ErrorCodeUnauthorized,
		})
		return
	}

	if c.GetHeader("Authorization") != "" {
		err := getUserInfo(h, c, &userInfo)
		if err != nil {
			return
		}

	}
	userId := c.Param("id")
	fmt.Println("ID: ", userId)
	resp, err := h.services.UserService().GetUserOrderHistory(context.Background(), &user_service.UserPrimaryKey{Id: userId})
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}
