package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/organization_service"
	"app/pkg/util"
)

// CreateCourier godoc
// @ID create_courier
// @Router /courier [POST]
// @Summary Create Courier
// @Description  Create Courier
// @Tags Courier
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body organization_service.CreateCourier true "CreateCourierRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Courier} "GetCourierBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCourier(c *gin.Context) {

	var courier organization_service.CreateCourier

	err := c.ShouldBindJSON(&courier)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().Create(
		c.Request.Context(),
		&courier,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetCourierByID godoc
// @ID get_courier_by_id
// @Router /courier/{id} [GET]
// @Summary Get Courier  By ID
// @Description Get Courier  By ID
// @Tags Courier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.Courier} "CourierBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCourierByID(c *gin.Context) {

	courierID := c.Param("id")

	if !util.IsValidUUID(courierID) {
		h.handleResponse(c, http.InvalidArgument, "courier id is an invalid uuid")
		return
	}

	resp, err := h.services.CourierService().GetByID(
		context.Background(),
		&organization_service.CourierPrimaryKey{
			Id: courierID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetCourierList godoc
// @ID get_courier_list
// @Router /courier [GET]
// @Summary Get Courier s List
// @Description  Get Courier s List
// @Tags Courier
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=organization_service.GetListCourierResponse} "GetAllCourierResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCourierList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.CourierService().GetList(
		context.Background(),
		&organization_service.GetListCourierRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateCourier godoc
// @ID update_courier
// @Router /courier/{id} [PUT]
// @Summary Update Courier
// @Description Update Courier
// @Tags Courier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateCourier true "UpdateCourierRequestBody"
// @Success 200 {object} http.Response{data=organization_service.Courier} "Courier data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCourier(c *gin.Context) {

	var courier organization_service.UpdateCourier

	courier.Id = c.Param("id")

	if !util.IsValidUUID(courier.Id) {
		h.handleResponse(c, http.InvalidArgument, "courier id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&courier)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CourierService().Update(
		c.Request.Context(),
		&courier,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteCourier godoc
// @ID delete_courier
// @Router /courier/{id} [DELETE]
// @Summary Delete Courier
// @Description Delete Courier
// @Tags Courier
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Courier data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCourier(c *gin.Context) {

	courierID := c.Param("id")

	if !util.IsValidUUID(courierID) {
		h.handleResponse(c, http.InvalidArgument, "courier id is an invalid uuid")
		return
	}

	resp, err := h.services.CourierService().Delete(
		c.Request.Context(),
		&organization_service.CourierPrimaryKey{Id: courierID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
