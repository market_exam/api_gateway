package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"app/api/http"
	"app/genproto/organization_service"
	"app/pkg/util"
)

// CreateSalePoint godoc
// @ID create_sale_point
// @Router /sale_point [POST]
// @Summary Create SalePoint
// @Description  Create SalePoint
// @Tags SalePoint
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body organization_service.CreateSalePoint true "CreateSalePointRequestBody"
// @Success 200 {object} http.Response{data=organization_service.SalePoint} "GetSalePointBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSalePoint(c *gin.Context) {

	var sale_point organization_service.CreateSalePoint

	err := c.ShouldBindJSON(&sale_point)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SalePointService().Create(
		c.Request.Context(),
		&sale_point,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetSalePointByID godoc
// @ID get_sale_point_by_id
// @Router /sale_point/{id} [GET]
// @Summary Get SalePoint  By ID
// @Description Get SalePoint  By ID
// @Tags SalePoint
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=organization_service.SalePoint} "SalePointBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalePointByID(c *gin.Context) {

	sale_pointID := c.Param("id")

	if !util.IsValidUUID(sale_pointID) {
		h.handleResponse(c, http.InvalidArgument, "sale_point id is an invalid uuid")
		return
	}

	resp, err := h.services.SalePointService().GetByID(
		context.Background(),
		&organization_service.SalePointPrimaryKey{
			Id: sale_pointID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetSalePointList godoc
// @ID get_sale_point_list
// @Router /sale_point [GET]
// @Summary Get SalePoint s List
// @Description  Get SalePoint s List
// @Tags SalePoint
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=organization_service.GetListSalePointResponse} "GetAllSalePointResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSalePointList(c *gin.Context) {

	// if c.GetHeader("role_id") == config.RoleClient {
	// 	h.handleResponse(c, http.OK, struct{}{})
	// 	return
	// }

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.SalePointService().GetList(
		context.Background(),
		&organization_service.GetListSalePointRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateSalePoint godoc
// @ID update_sale_point
// @Router /sale_point/{id} [PUT]
// @Summary Update SalePoint
// @Description Update SalePoint
// @Tags SalePoint
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body organization_service.UpdateSalePoint true "UpdateSalePointRequestBody"
// @Success 200 {object} http.Response{data=organization_service.SalePoint} "SalePoint data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSalePoint(c *gin.Context) {

	var sale_point organization_service.UpdateSalePoint

	sale_point.Id = c.Param("id")

	if !util.IsValidUUID(sale_point.Id) {
		h.handleResponse(c, http.InvalidArgument, "sale_point id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&sale_point)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.SalePointService().Update(
		c.Request.Context(),
		&sale_point,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteSalePoint godoc
// @ID delete_sale_point
// @Router /sale_point/{id} [DELETE]
// @Summary Delete SalePoint
// @Description Delete SalePoint
// @Tags SalePoint
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SalePoint data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSalePoint(c *gin.Context) {

	sale_pointID := c.Param("id")

	if !util.IsValidUUID(sale_pointID) {
		h.handleResponse(c, http.InvalidArgument, "sale_point id is an invalid uuid")
		return
	}

	resp, err := h.services.SalePointService().Delete(
		c.Request.Context(),
		&organization_service.SalePointPrimaryKey{Id: sale_pointID},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
