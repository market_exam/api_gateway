package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"app/api/docs"
	"app/api/handlers"
	"app/config"
)

// New
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.GET("/config", h.GetConfig)

	// Product Service - Category
	r.POST("/category", h.CreateCategory)
	r.GET("/category/:id", h.GetCategoryByID)
	r.GET("/category", h.GetCategoryList)
	r.PUT("/category/:id", h.UpdateCategory)
	r.DELETE("/category/:id", h.DeleteCategory)

	// Product Service - Product
	r.POST("/product", h.CreateProduct)
	r.GET("/product/:id", h.GetProductByID)
	r.GET("/product", h.GetProductList)
	r.PUT("/product/:id", h.UpdateProduct)
	r.DELETE("/product/:id", h.DeleteProduct)

	// STock Service - Ostatok
	r.POST("/ostatok", h.CreateOstatok)
	r.GET("/ostatok/:id", h.GetOstatokByID)
	r.GET("/ostatok", h.GetOstatokList)
	r.PUT("/ostatok/:id", h.UpdateOstatok)
	r.DELETE("/ostatok/:id", h.DeleteOstatok)
	r.GET("/ostatok_product_id/:id", h.GetOstatokByProductId)

	// stock Service - Coming
	r.POST("/coming", h.CreateComing)
	r.GET("/coming/:id", h.GetComingByID)
	r.GET("/coming", h.GetComingList)
	r.PUT("/coming/:id", h.UpdateComing)
	r.DELETE("/coming/:id", h.DeleteComing)

	// Ostatok Service - ComingProduct
	r.POST("/comingProduct", h.CreateComingProduct)
	r.GET("/comingProduct/:id", h.GetComingProductByID)
	r.GET("/comingProduct", h.GetComingProductList)
	r.PUT("/comingProduct/:id", h.UpdateComingProduct)
	r.DELETE("/comingProduct/:id", h.DeleteComingProduct)

	// Organisation Service - Filial
	r.POST("/filial", h.CreateFilial)
	r.GET("/filial/:id", h.GetFilialByID)
	r.GET("/filial", h.GetFilialList)
	r.PUT("/filial/:id", h.UpdateFilial)
	r.DELETE("/filial/:id", h.DeleteFilial)

	// Organisation Service - SalePoint
	r.POST("/sale_point", h.CreateSalePoint)
	r.GET("/sale_point/:id", h.GetSalePointByID)
	r.GET("/sale_point", h.GetSalePointList)
	r.PUT("/sale_point/:id", h.UpdateSalePoint)
	r.DELETE("/sale_point/:id", h.DeleteSalePoint)

	// Organisation Service - employee
	r.POST("/employee", h.CreateEmployee)
	r.GET("/employee/:id", h.GetEmployeeByID)
	r.GET("/employee", h.GetEmployeeList)
	r.PUT("/employee/:id", h.UpdateEmployee)
	r.DELETE("/employee/:id", h.DeleteEmployee)

	// Organisation Service - courier
	r.POST("/courier", h.CreateCourier)
	r.GET("/courier/:id", h.GetCourierByID)
	r.GET("/courier", h.GetCourierList)
	r.PUT("/courier/:id", h.UpdateCourier)
	r.DELETE("/courier/:id", h.DeleteCourier)

	// Kassa Service - smena
	r.POST("/smena", h.CreateSmena)
	r.GET("/smena/:id", h.GetSmenaByID)
	r.GET("/smena", h.GetSmenaList)
	r.PUT("/smena/:id", h.UpdateSmena)
	r.DELETE("/smena/:id", h.DeleteSmena)
	r.PUT("/close_smena/:id", h.CloseSmena)

	// Kassa Service - transaction
	r.POST("/transaction", h.CreateTransaction)
	r.GET("/transaction/:id", h.GetTransactionByID)
	r.GET("/transaction", h.GetTransactionList)
	r.DELETE("/transaction/:id", h.DeleteTransaction)

	// Kassa Service - sale
	r.POST("/sale", h.CreateSale)
	r.GET("/sale/:id", h.GetSaleByID)
	r.GET("/sale", h.GetSaleList)
	r.PUT("/sale/:id", h.UpdateSale)
	r.DELETE("/sale/:id", h.DeleteSale)

	// Kassa Service - saleProduct
	r.POST("/saleProduct", h.CreateSaleProduct)
	r.GET("/saleProduct/:id", h.GetSaleProductByID)
	r.GET("/saleProduct", h.GetSaleProductList)
	r.PUT("/saleProduct/:id", h.UpdateSaleProduct)
	r.DELETE("/saleProduct/:id", h.DeleteSaleProduct)

	// Kassa Service - salePayment
	r.POST("/salePayment", h.CreateSalePayment)
	r.GET("/salePayment/:id", h.GetSalePaymentByID)
	r.GET("/salePayment", h.GetSalePaymentList)
	r.PUT("/salePayment/:id", h.UpdateSalePayment)
	r.DELETE("/salePayment/:id", h.DeleteSalePayment)

	// Business Events - Coming_To_Stock
	r.POST("/coming_to_stock/:id", h.Coming_to_Stock)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
