package models

type UserInfo struct {
	ID     string `json:"id"`
	UserID string `json:"user_id"`
}
